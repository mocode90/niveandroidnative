package com.nivea.foodapp.android.Utils;

/**
 * Created by foodapp on 1/4/2018.
 */

public enum SwipeDirection {
    all, left, right, none ;
}
