package com.nivea.foodapp.android.GoogleMapWork;

import java.util.ArrayList;

/**
 * Created by foodapp on 3/13/2018.
 */

interface SavedPlaceListener {
    public void onSavedPlaceClick(ArrayList<SavedAddress> mResultList, int position);
}
