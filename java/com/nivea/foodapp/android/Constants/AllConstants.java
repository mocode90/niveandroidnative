package com.nivea.foodapp.android.Constants;

/**
 * Created by foodapp on 12/12/2017.
 */

public class AllConstants {

    public static int width=0;
    public static int height=0;

    public static String verdana = "verdana.ttf";
    public static String arial = "arial.ttf";

    public static final String CALCULATION = "CalculationAndroid";
    public static final String TRACKING = "tracking";

    public static final String FIREBASE_TOKEN = "firebaseTocken";

    public static String textTypeFaceBold = "fonts/OpenSans-Bold.ttf";
    public static String textTypeFaceSemiBold = "fonts/OpenSans-SemiBold.ttf";
}
