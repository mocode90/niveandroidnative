package com.nivea.foodapp.android.Models;

/**
 * Created by foodapp on 1/5/2018.
 */

public class ParentModel {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    String name,amount;

}
