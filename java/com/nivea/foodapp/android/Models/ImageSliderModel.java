package com.nivea.foodapp.android.Models;

/**
 * Created by foodapp on 12/26/2017.
 */

public class ImageSliderModel {

    String sliderImageUrl;

    public String getSliderImageUrl() {
        return sliderImageUrl;
    }

    public void setSliderImageUrl(String sliderImageUrl) {
        this.sliderImageUrl = sliderImageUrl;
    }
}
