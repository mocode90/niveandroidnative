package com.nivea.foodapp.android.Models;

/**
 * Created by foodapp on 1/5/2018.
 */

public class ChildModel {

    public String getExtra_menu_item() {
        return extra_menu_item;
    }

    public void setExtra_menu_item(String extra_menu_item) {
        this.extra_menu_item = extra_menu_item;
    }

    String extra_menu_item;
}
