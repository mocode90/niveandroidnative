package com.nivea.foodapp.android.Models;

/**
 * Created by foodapp on 1/10/2018.
 */

public class RestaurantParentModel {

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    String title,sub_title;

}
